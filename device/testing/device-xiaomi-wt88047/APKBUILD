# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-xiaomi-wt88047
pkgdesc="Xiaomi Redmi 2"
pkgver=2
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base mkbootimg soc-qcom-msm8916"
makedepends="devicepkg-dev"
source="deviceinfo"
subpackages="
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-kernel-mainline-modem-wt86047:kernel_mainline_modem_wt86047
	$pkgname-kernel-mainline-modem-wt88047:kernel_mainline_modem_wt88047
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-nonfree-firmware-modem-wt86047:nonfree_firmware_modem_wt86047
	$pkgname-nonfree-firmware-modem-wt88047:nonfree_firmware_modem_wt88047
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel (no modem, audio routed directly)"
	depends="linux-postmarketos-qcom-msm8916"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

# wt86047 (Chinese variant) and wt88047 (global variant) need different
# modem firmware, otherwise they are pretty much the same.

kernel_mainline_modem_wt86047() {
	pkgdesc="Close to mainline kernel (wt86047 (China), non-free modem, audio routed through ADSP)"
	depends="linux-postmarketos-qcom-msm8916 soc-qcom-msm8916-modem"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline_modem_wt88047() {
	pkgdesc="Close to mainline kernel (wt88047 (global), non-free modem, audio routed through ADSP)"
	depends="linux-postmarketos-qcom-msm8916 soc-qcom-msm8916-modem"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="GPU/WiFi/BT/Video(/Modem) firmware"
	depends="linux-firmware-qcom firmware-qcom-msm8916-wcnss firmware-xiaomi-wt88047-wcnss-nv"
	mkdir "$subpkgdir"
}

nonfree_firmware_modem_wt86047() {
	pkgdesc="Modem firmware (WT86047)"
	depends="firmware-xiaomi-wt86047-modem"
	install_if="$pkgname-nonfree-firmware $pkgname-kernel-mainline-modem-wt86047"
	mkdir "$subpkgdir"
}

nonfree_firmware_modem_wt88047() {
	pkgdesc="Modem firmware (WT88047)"
	depends="firmware-xiaomi-wt88047-modem"
	install_if="$pkgname-nonfree-firmware $pkgname-kernel-mainline-modem-wt88047"
	mkdir "$subpkgdir"
}

sha512sums="1e2132f97fa32ff457cd930af79410e0daef0b32382af2da259a8f0962d050d611ba3d3478a1afd48a38f5f47a746e05241e18c1835ee190854bd09c519c7014  deviceinfo"
